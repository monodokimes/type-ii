﻿using UnityEngine;
using System.Collections;

public class Resource : MonoBehaviour {

    public int Amount { get; set; }
    public string Name { get; set; }
    public string Unit { get; set; }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
