﻿using UnityEngine;
using System.Collections;

public class OrbitalComponent : MonoBehaviour {

    float slowTimeFactor = 0.001f;

    float angle = 0;
    float speed;
    float radius;

	// Use this for initialization
	void Start () {
        radius = Vector3.Distance(transform.position, transform.parent.position);
        var period = Mathf.Pow(radius, 1f / 3f);
        speed = 2 * Mathf.PI / period;
        angle = Random.Range(0, 360);
	}
	
	// Update is called once per frame
	void Update () {
        angle += speed * Time.deltaTime * slowTimeFactor;
        transform.position = new Vector3(Mathf.Cos(angle) * radius, 0, Mathf.Sin(angle) * radius);
	}

    
}
