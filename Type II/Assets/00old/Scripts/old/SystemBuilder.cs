﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class SystemBuilder : MonoBehaviour {

    public GameObject starPrefab;
    public GameObject planetPrefab;
    public int numberOfPlanets = 10;
    public float minPlanetSpacing = 10;
    public float maxPlanetSpacing = 100;
    public float minPlanetRadius = 1;
    public float maxPlanetRadius = 10;


    // Use this for initialization
    void Start() {
        // currently can only place a star at origin
        var star = (GameObject)Instantiate(starPrefab, new Vector3(), new Quaternion());
        star.transform.parent = transform;
        star.name = "Sol";

        float distance = 0;

        for (int i = 0; i < numberOfPlanets; i++) {
            distance += Random.Range(minPlanetSpacing, maxPlanetSpacing);

            var planet = (GameObject)Instantiate(planetPrefab, new Vector3(distance, 0, 0), new Quaternion());
            planet.transform.localScale *= Random.Range(minPlanetRadius, maxPlanetRadius);
            planet.transform.parent = transform;
            planet.name = string.Join("-", new[] { star.name, (i + 1).ToString() });
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
