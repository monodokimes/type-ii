﻿using UnityEngine;
using System.Collections;

public class DockingPoint : MonoBehaviour {

    public Vector3 Top {
        get {
            return new Vector3(
                transform.position.x, 
                transform.localScale.y / 2 + transform.position.y, 
                transform.position.z);
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
