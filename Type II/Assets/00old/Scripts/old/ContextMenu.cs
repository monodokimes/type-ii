﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ContextMenuItem {
    // this class - just a box to some data

    public string text;             // text to display on button
    public Button button;           // sample button prefab
    public Action<Image> action;    // delegate to method, that need to be executed when button is clicked

    public ContextMenuItem(string text, Button button, Action<Image> action) {
        this.text = text;
        this.button = button;
        this.action = action;
    }
}

public class ContextMenu : MonoBehaviour {
    public Vector2 position;
    public Vector2 size;

    public Image contentPanel;              // content panel prefab
    public Image namePanel;
    public Canvas canvas;                   // link to main canvas, where will be Context Menu

    private static ContextMenu instance;    // some kind of singletone here

    private readonly KeyCode[] CONTEXT_MENU_KILLERS = new KeyCode[] {
        KeyCode.Q,
        KeyCode.W,
        KeyCode.E,
        KeyCode.A,
        KeyCode.S,
        KeyCode.D,
        KeyCode.LeftShift,
        KeyCode.LeftControl
    };

    public static ContextMenu Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType(typeof(ContextMenu)) as ContextMenu;

                if (instance == null) 
                    instance = new ContextMenu();
            }
            return instance;
        }
    }

    void LateUpdate() {
        if (Input.GetMouseButtonUp(0)) 
            DestroyContextMenu();
        
        foreach (var keyCode in CONTEXT_MENU_KILLERS)
            if (Input.GetKeyDown(keyCode))
                DestroyContextMenu();
    }

    public static void DestroyContextMenu() {
        var menu = FindObjectOfType<Canvas>().transform.Find("ContentPanel(Clone)");
        if (menu) 
            Destroy(menu.gameObject);
    }

    public void CreateContextMenu(List<ContextMenuItem> contextMenuItems, Vector2 position) {
        this.position = position;
        size = new Vector2();


        Image panel = Instantiate(
            contentPanel,
            new Vector3(
                position.x,
                position.y, 0),
        Quaternion.identity) as Image;

        panel.transform.SetParent(canvas.transform);
        panel.transform.SetAsLastSibling();
        panel.rectTransform.anchoredPosition = position;

        foreach (var contextMenuItem in contextMenuItems) {
            ContextMenuItem tempReference = contextMenuItem;
            Button button = Instantiate(contextMenuItem.button) as Button;
            Text buttonText = button.GetComponentInChildren(typeof(Text)) as Text;

            buttonText.text = contextMenuItem.text;
            button.onClick.AddListener(delegate { tempReference.action(panel); });
            button.transform.SetParent(panel.transform);
            var rect = button.GetComponent<RectTransform>().rect;
            size.x = rect.width;
            size.y += rect.height;
        }
    }
}
