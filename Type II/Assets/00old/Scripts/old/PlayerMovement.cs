﻿using UnityEngine;
using System.Collections;
using System;

[DisallowMultipleComponent]
public class PlayerMovement : MonoBehaviour {
    [SerializeField]
    [Range(1, 20)]
    private float speed = 10;
    public float OrbitalHeight;

    private Vector3 targetPosition;

    private bool isMoving;

    GameObject targetObject;

    const int RIGHT_MOUSE_BUTTON = 1;
    // Use this for initialization
    void Start() {
        targetPosition = transform.position;
        isMoving = false;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButton(RIGHT_MOUSE_BUTTON)) {
            //SetTargetPosition();
        }

        if (isMoving) 
            MoveShip();
        else {
            transform.position = GetPointAboveDockable(targetObject);
        }
    }

    Vector3 GetPointAboveDockable(GameObject target) {
        if (target != null && target.GetComponent<DockingPoint>() != null) {
            var pos = targetObject.GetComponent<DockingPoint>().Top;
            pos.y += OrbitalHeight;
            return pos;
        }
        return transform.position;
    }

    public void SetTargetPosition(GameObject targetObject) {
        this.targetObject = targetObject;
        targetPosition = GetPointAboveDockable(targetObject);
        if (targetObject.GetComponent<DockingPoint>() != null)
            isMoving = true;

        return;
    }

    private void SetTargetPosition() {
        // Gets object from RaycastHit, ray cast from screen outwards
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;

        if (Physics.Raycast(ray, out raycastHit)) {
            var gameObject = raycastHit.transform.parent.gameObject;
            SetTargetPosition(gameObject);
        }

    }

    private void MoveShip() {
        transform.LookAt(targetPosition);
        transform.position = Vector3.MoveTowards(
            transform.position,
            targetPosition,
            speed * Time.deltaTime);

        // Stop moving when arrived at destination
        if (transform.position == targetPosition)
            isMoving = false;
        else {
            SetTargetPosition(targetObject);
        }
        Debug.DrawLine(transform.position, targetPosition, Color.red);
    }
}
