﻿using UnityEngine;
using System.Collections;
using System;

public class CameraMovement : MonoBehaviour {

    public float speed;
    public float rotationSpeed;

    float mouseX;
    float mouseY;

    bool verticalRotationEnabled = true;
    float minVerticalRotation = 0f;
    float maxVerticalRotation = 65f;

    public float lookAtStarDistance;

	// Use this for initialization
	void Start () {
        
	}
	
	void LateUpdate () {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) {
            transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) {
            transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.E)) {
            transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.Q)) {
            transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
        }
        HandleMouseRotation();

        mouseX = Input.mousePosition.x;
        mouseY = Input.mousePosition.y;
    }

    private void HandleMouseRotation() {
        var easeFactor = 10f;
        if (Input.GetMouseButton(0)) {
            // Horizontal rotation
            if (Input.mousePosition.x != mouseX) {
                var cameraRotationY = (Input.mousePosition.x - mouseX) * easeFactor * Time.deltaTime;
                transform.Rotate(0, cameraRotationY, 0);
            }

            // Vertical rotation
            if (verticalRotationEnabled) {
                Camera mainCamera = transform.gameObject.GetComponentInChildren<Camera>();
                var cameraRotationX = (mouseY - Input.mousePosition.y) * easeFactor * Time.deltaTime;
                var desiredRotationX = mainCamera.transform.eulerAngles.x + cameraRotationX;

                if (desiredRotationX >= minVerticalRotation && desiredRotationX <= maxVerticalRotation) {
                    mainCamera.transform.Rotate(cameraRotationX, 0, 0);
                }
            }

        }

    }
}
