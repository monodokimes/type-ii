﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemController : MonoBehaviour {
    public Button sampleButton;
    public Text sampleText;
    public Image panel;

    private List<ContextMenuItem> contextMenuItems;

    bool isOpen;
    bool isOverObject;

    string gameObjectName = string.Empty;

    private Rect MenuRect {
        get {
            return new Rect(ContextMenu.Instance.position, ContextMenu.Instance.size);
        }
    }

    // Populate context menu based on gameObject's components
    void Awake() {
        contextMenuItems = new List<ContextMenuItem>();
            
        if (gameObject.GetComponent<DockingPoint>())
            contextMenuItems.Add(new ContextMenuItem("Navigate to " + gameObjectName, sampleButton, i => NavigateAction(i)));
    }

    void OnMouseEnter() {
        isOverObject = true;

        if (!isOpen) {
            isOpen = true;
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            ContextMenu.Instance.CreateContextMenu(contextMenuItems, new Vector2(pos.x, pos.y));
        }
    }

    void OnGUI() {
        if (gameObject.name != gameObjectName) {
            gameObjectName = gameObject.name;
            Awake();
        }
        if (isOpen && !isOverObject && !CursorOverMenu())
            CloseMenu();
    }

    bool CursorOverMenu() {
        return MenuRect.Contains(Input.mousePosition);
    }

    private void CloseMenu() {
        isOpen = false;
        ContextMenu.DestroyContextMenu();
    }

    void OnMouseExit() {
        isOverObject = false;
        if (!CursorOverMenu()) 
            CloseMenu();
    }

    private void NavigateAction(Image contextPanel) {
        var player = GameObject.FindGameObjectWithTag("Player");
        var playerMovementComponent = player.GetComponent<PlayerMovement>();
        
        playerMovementComponent.SetTargetPosition(gameObject);
        Destroy(contextPanel.gameObject);
    }
}
