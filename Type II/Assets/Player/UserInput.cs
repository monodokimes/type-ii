﻿using UnityEngine;
using System.Collections;
using System;
using TypeII;

public class UserInput : MonoBehaviour {

    Player player;

    // Use this for initialization
    void Start() {
        player = transform.root.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update() {
        if (player.human) {
            CameraMovement();
            CameraRotation();
            HandleMouseButtonClick();
            MouseHover();
        }
    }

    void MouseHover() {
        if (player.hud.MouseInBounds()) {
            GameObject hoverObject = FindHitObject();
            if (hoverObject) {
                if (player.SelectedObject) {
                    player.SelectedObject.SetHoverState(hoverObject);
                } else if (hoverObject.name != "Ground") {
                    Player owner = hoverObject.transform.root.GetComponent<Player>();
                    if (owner) {
                        Unit unit = hoverObject.transform.parent.GetComponent<Unit>();
                        Building building = hoverObject.transform.parent.GetComponent<Building>();
                        if (owner.username == player.username && (unit || building)) {
                            player.hud.SetCursorState(CursorState.Move);
                        }
                    }
                }
            }
        }
    }

    Vector3 FindHitPoint() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
            return hit.point;
        return ResourceManager.InvalidPosition;
    }

    GameObject FindHitObject() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
            return hit.collider.gameObject;
        return null;
    }

    void HandleMouseButtonClick() {
        if (Input.GetMouseButtonDown(0))
            LeftMouseClick();
        else if (Input.GetMouseButtonDown(1))
            RightMouseClick();
        else if (Input.GetMouseButtonDown(2)) {
            MiddleMouseClick();
        }

    }

    private void MiddleMouseClick() {
        throw new NotImplementedException();
    }

    private void RightMouseClick() {
        if (player.hud.MouseInBounds() && !Input.GetKey(KeyCode.LeftControl) && player.SelectedObject) {
            player.SelectedObject.SetSelection(false, player.hud.GetPlayingArea());
            player.SelectedObject = null;
        }
    }

    private void LeftMouseClick() {
        if (player.hud.MouseInBounds()) {
            GameObject hitObject = FindHitObject();
            Vector3 hitPoint = FindHitPoint();
            if (hitObject && hitPoint != ResourceManager.InvalidPosition) {
                if (player.SelectedObject) {
                    player.SelectedObject.MouseClick(hitObject, hitPoint, player);
                } else if (hitObject.name != "Ground") {
                    WorldObject worldObject = hitObject.transform.parent.GetComponent<WorldObject>();
                    if (worldObject) {
                        player.SelectedObject = worldObject;
                        worldObject.SetSelection(true, player.hud.GetPlayingArea());
                    }
                }
            }
        }
    }

    void CameraMovement() {
        float mouseX = Input.mousePosition.x;
        float mouseY = Input.mousePosition.y;
        Vector3 movement = new Vector3();

        bool mouseScroll = false;

        // Horizontal camera movement
        if (mouseX >= 0 && mouseX < ResourceManager.ScrollWidth) {
            movement.x -= ResourceManager.ScrollSpeed;
            player.hud.SetCursorState(CursorState.PanLeft);
            mouseScroll = true;
        } else if (mouseX <= Screen.width && mouseX > Screen.width - ResourceManager.ScrollWidth) {
            movement.x += ResourceManager.ScrollSpeed;
            player.hud.SetCursorState(CursorState.PanRight);
            mouseScroll = true;
        }

        // Vertical camera movement
        if (mouseY >= 0 && mouseY < ResourceManager.ScrollWidth) {
            movement.z -= ResourceManager.ScrollSpeed;
            player.hud.SetCursorState(CursorState.PanDown);
            mouseScroll = true;
        } else if (mouseY <= Screen.height && mouseY > Screen.height - ResourceManager.ScrollWidth) {
            movement.z += ResourceManager.ScrollSpeed;
            player.hud.SetCursorState(CursorState.PanUp);
            mouseScroll = true;
        }
        // Keep the camera straight
        movement = Camera.main.transform.TransformDirection(movement);

        movement.y = ResourceManager.ScrollSpeed * Input.GetAxis("Mouse ScrollWheel");

        Vector3 origin = Camera.main.transform.position;
        Vector3 destination = origin + movement;

        if (destination.y > ResourceManager.MaxCameraHeight)
            destination.y = ResourceManager.MaxCameraHeight;
        else if (destination.y < ResourceManager.MinCameraHeight)
            destination.y = ResourceManager.MinCameraHeight;

        if (destination != origin)
            Camera.main.transform.position = Vector3.MoveTowards(
                origin,
                destination,
                Time.deltaTime * ResourceManager.ScrollSpeed);

        if (!mouseScroll) {
            player.hud.SetCursorState(CursorState.Select);
        }
    }

    private void CameraRotation() {
        Vector3 origin = Camera.main.transform.eulerAngles;
        Vector3 destination = origin;

        if (Input.GetKey(KeyCode.LeftControl) &&
            Input.GetMouseButton(1)) {

            destination.x -= Input.GetAxis("Mouse Y") * ResourceManager.RotateAmount;
            destination.y += Input.GetAxis("Mouse X") * ResourceManager.RotateAmount;
        }

        if (destination != origin)
            Camera.main.transform.eulerAngles = Vector3.MoveTowards(
                origin,
                destination,
                Time.deltaTime * ResourceManager.RotateSpeed);
    }
}
