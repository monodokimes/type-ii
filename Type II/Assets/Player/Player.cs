﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TypeII;
using System;

public class Player : MonoBehaviour {

    public string username;
    public bool human;
    public HUD hud;

    public int
        startMoney,
        startMoneyLimit,
        startPower,
        startPowerLimit;

    Dictionary<ResourceType, int> resources, resourceLimits;

    public WorldObject SelectedObject { get; set; }

    // Use this for initialization

    void Awake() {
        resources = InitResourceList();
        resourceLimits = InitResourceList();
    }

    void Start () {
        hud = GetComponentInChildren<HUD>();
        AddStartResourceLimits();
        AddStartResources();
	}

    // Update is called once per frame
    void Update () {
        if (human) {
            hud.SetResourceValues(resources, resourceLimits);
        }
	}

    public void AddUnit(string unitName, Vector3 spawnPoint, Quaternion rotation) {
        Debug.Log("add " + unitName + " to " + username);
        Units units = GetComponentInChildren<Units>();
        GameObject newUnit = Instantiate(ResourceManager.GetUnit(unitName));
        newUnit.transform.parent = units.transform;
    }

    private Dictionary<ResourceType, int> InitResourceList() {
        Dictionary<ResourceType, int> list = new Dictionary<ResourceType, int>();
        list.Add(ResourceType.Money, 0);
        list.Add(ResourceType.Power, 0);
        return list;
    }

    private void AddStartResources() {
        AddResource(ResourceType.Money, startMoney);
        AddResource(ResourceType.Power, startPower);
    }

    private void AddStartResourceLimits() {
        IncrementResourceLimit(ResourceType.Money, startMoneyLimit);
        IncrementResourceLimit(ResourceType.Power, startPowerLimit);
    }

    public void AddResource(ResourceType type, int amount) {
        resources[type] += amount;
    }

    public void IncrementResourceLimit(ResourceType type, int amount) {
        resourceLimits[type] += amount;
    }
}
