﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TypeII;

public class Building : WorldObject {
    public float maxBuildProgress;
    public Texture2D rallyPointImage;

    protected Queue<string> buildQueue;
    protected Vector3 rallyPoint;

    float currentBuildProgress = 0f;
    Vector3 spawnPoint;

    protected override void Awake() {
        base.Awake();

        rallyPoint = spawnPoint;
        buildQueue = new Queue<string>();
        float spawnX = selectionBounds.center.x + transform.forward.x * selectionBounds.extents.x + transform.forward.x * 10;
        float spawnY = selectionBounds.center.z + transform.forward.z + selectionBounds.extents.z + transform.forward.z * 10;
        spawnPoint = new Vector3(spawnX, 0f, spawnY);
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();

        ProcessBuildQueue();
    }

    protected override void OnGUI() {
        base.OnGUI();
    }

    public override void SetSelection(bool selected, Rect playingArea) {
        base.SetSelection(selected, playingArea);

        if (player) {
            RallyPoint flag = player.GetComponentInChildren<RallyPoint>();
            if (selected) {
                if (flag && player.human && spawnPoint != ResourceManager.InvalidPosition) {
                    flag.transform.localPosition = rallyPoint;
                    flag.transform.forward = transform.forward;
                    flag.Enable();
                } else {
                    if (flag && player.human) {
                        flag.Disable();
                    }
                }
            }
        }
    }

    public string[] GetBuildQueueValues() {
        string[] values = new string[buildQueue.Count];
        int pos = 0;
        foreach (string unit in buildQueue) {
            values[pos++] = unit;
        }
        return values;
    }

    public float GetBuildPercentage() {
        return currentBuildProgress / maxBuildProgress;
    }

    protected void CreateUnit(string unitName) {
        buildQueue.Enqueue(unitName);
    }

    private void ProcessBuildQueue() {
        if (buildQueue.Count > 0) {
            currentBuildProgress += Time.deltaTime * ResourceManager.BuildSpeed;
                if (currentBuildProgress > maxBuildProgress) {
                if (player) {
                    player.AddUnit(buildQueue.Dequeue(), spawnPoint, transform.rotation);
                }
                currentBuildProgress = 0;
            }
        }
    }

    public bool HasSpawnPoint() {
        return spawnPoint != ResourceManager.InvalidPosition && rallyPoint != ResourceManager.InvalidPosition;
    }
}
