﻿using UnityEngine;
using System.Collections;
using System;
using TypeII;

public class WorldObject : MonoBehaviour {

    public string objectName;
    public Texture2D buildImage;
    public int 
        cost, 
        sellvalue, 
        hitPoints, 
        maxHitPoints;

    protected Player player;
    protected Bounds selectionBounds;

    protected string[] actions = { };
    protected bool currentlySelected = false;
    protected Rect playingArea = new Rect(0, 0, 0, 0);

    protected virtual void Awake() {
        selectionBounds = ResourceManager.InvalidBounds;
        CalculateBounds();
    }

    protected virtual void Start() {
        player = transform.root.GetComponentInChildren<Player>();
    }

    protected virtual void Update() {

    }

    protected virtual void OnGUI() {
        if (currentlySelected) {
            DrawSelection();
        }
    }

    public void CalculateBounds() {
        selectionBounds = new Bounds(transform.position, Vector3.zero);
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
            selectionBounds.Encapsulate(renderer.bounds);
    }

    public virtual void SetHoverState(GameObject hoverObject) {
        if (OwnedAndSelected()) {
            if (hoverObject.name=="Ground") {
                player.hud.SetCursorState(CursorState.Move);
            }
        }
    }

    protected bool OwnedAndSelected() {
        return player && player.human && currentlySelected;
    }

    private void DrawSelection() {
        GUI.skin = ResourceManager.SelectBoxSkin;
        Rect selectBox = WorkManager.CalculateSelectionBox(selectionBounds, playingArea);
        GUI.BeginGroup(playingArea);
        DrawSelectionBox(selectBox);
        GUI.EndGroup();
    }

    private void DrawSelectionBox(Rect selectBox) {
        GUI.Box(selectBox, "");
    }

    public virtual void MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller) {
        if (currentlySelected && hitObject && hitObject.name != "Ground") {
            WorldObject worldObject = hitObject.transform.parent.GetComponent<WorldObject>();
            if (worldObject)
                ChangeSelection(worldObject, controller);
        }
    }

    private void ChangeSelection(WorldObject worldObject, Player controller) {
        SetSelection(false, playingArea);
        if (controller.SelectedObject)
            controller.SelectedObject.SetSelection(false, playingArea);
        controller.SelectedObject = worldObject;
        worldObject.SetSelection(true, controller.hud.GetPlayingArea());
    }

    public virtual void SetSelection(bool selected, Rect playingArea) {
        currentlySelected = selected;
        if (selected) 
            this.playingArea = playingArea;
    }

    public string[] GetActions() {
        return actions;
    }

    public virtual void PerformAction(string actionToPerform) {

    }

    public bool IsOwnedBy(Player owner) {
        if (player && player.Equals(owner)) {
            return true;
        } else {
            return false;
        }
    }
}
